package com.rakibul.haque.whatuwant;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rakibul.haque.whatuwant.Prevalent.Prevalent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MessageActivity extends AppCompatActivity implements LocationListener {

    String RequestedWorker;
    EditText message;
    Button Send,Location;


    RadioGroup radioGroup;
    RadioButton radioButton;
    DatabaseReference mref;
    private ProgressDialog LoadingBar;

    private LocationManager locationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        Location=(Button)findViewById(R.id.location);

        locationManager=(LocationManager)getSystemService(Context.LOCATION_SERVICE);

        RequestedWorker=getIntent().getStringExtra("RequestedWorker");

        Send=(Button)findViewById(R.id.send);
        message=(EditText)findViewById(R.id.sms);

        LoadingBar=new ProgressDialog(this);
        mref= FirebaseDatabase.getInstance().getReference();

        radioGroup=(RadioGroup)findViewById(R.id.radioGroup);

        Location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(ActivityCompat.checkSelfPermission(MessageActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(MessageActivity.this,Manifest.permission.ACCESS_COARSE_LOCATION) !=PackageManager.PERMISSION_GRANTED)
                {

                    return;
                }
                Location location=locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);
                onLocationChanged(location);

            }
        });




        mref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                if ( dataSnapshot.child("Users").child(Prevalent.currentOnlineUser.getPhone()).child("Address").getValue().equals(""))
                {

                }
                else
                {
                    final AlertDialog.Builder builder=new AlertDialog.Builder(MessageActivity.this);
                    builder.setTitle("WUW Assistant!");
                    builder.setMessage("Do You Want To Use Address that you use Last Time");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            mref.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                    String myadd=dataSnapshot.child("Users").child(Prevalent.currentOnlineUser.getPhone()).child("Address").getValue().toString();
                                    message.setText(myadd);
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.cancel();
                        }
                    });
                    builder.show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        Send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(TextUtils.isEmpty(message.getText().toString()))
                {
                    Toast.makeText(MessageActivity.this, "Please provide your Address..", Toast.LENGTH_LONG).show();
                }
                else
                {
                    if(Prevalent.currentOnlineUser.getPhone().equals("+911111111111"))
                    {
                        Intent i=new Intent(MessageActivity.this,LoginActivity.class);
                        startActivity(i);
                        Toast.makeText(MessageActivity.this, "Login First..", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        int radioId=radioGroup.getCheckedRadioButtonId();
                        radioButton=findViewById(radioId);


                        LoadingBar.setTitle("Please Wait..");
                        LoadingBar.setMessage("Sending message..");
                        LoadingBar.show();

                        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                        Date today = new Date();
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                        String dateToStr = format.format(today);
                        //System.out.println(dateToStr);

                        String SMSmessage=message.getText().toString();
                        String send="My name is "+ Prevalent.currentOnlineUser.getName()+" i need a "+RequestedWorker
                                +" at address "+SMSmessage+" and i complete payment by "+radioButton.getText().toString()
                                +" mode and my phone number is "+Prevalent.currentOnlineUser.getPhone();

                        Map<String,Object> map=new HashMap<String,Object>();
                        map.put(Prevalent.currentOnlineUser.getPhone()+System.currentTimeMillis(),dateToStr+System.currentTimeMillis()+"\n"+send);

                        mref.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                dataSnapshot.getRef().child("Users").child(Prevalent.currentOnlineUser.getPhone()).child("Address").setValue(message.getText().toString().trim());
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        mref.child("Services").updateChildren(map)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                        if(task.isSuccessful())
                                        {
                                            LoadingBar.dismiss();
                                        }
                                        else
                                        {
                                            LoadingBar.dismiss();
                                            Toast.makeText(MessageActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });

                        Intent smsIntent = new Intent(Intent.ACTION_VIEW);

                        smsIntent.setData(Uri.parse("smsto:"));
                        smsIntent.setType("vnd.android-dir/mms-sms");
                        smsIntent.putExtra("address"  , new String ("83717502"));
                        smsIntent.putExtra("sms_body"  ,  "My name is "+ Prevalent.currentOnlineUser.getName()+" i need a "+RequestedWorker
                                +" at address "+SMSmessage+" and i complete payment by "+radioButton.getText().toString()
                                +" mode and my phone number is "+Prevalent.currentOnlineUser.getPhone());

                        try {
                            startActivity(smsIntent);

                            finish();
                            Log.i("Finished sending SMS...", "");
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(MessageActivity.this,
                                    "SMS faild, please try again later.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    /** if(checkPermission(Manifest.permission.SEND_SMS))
                     {

                     SmsManager smsManager=SmsManager.getDefault();
                     smsManager.sendTextMessage("8439321860",null,"My name is "+ Prevalent.currentOnlineUser.getName()+" i need a "+RequestedWorker
                     +" at address "+SMSmessage+" and i complete payment by "+radioButton.getText().toString()
                     +" mode and my phone number is "+Prevalent.currentOnlineUser.getPhone(),null,null);
                     Toast.makeText(MessageActivity.this, "message Send", Toast.LENGTH_LONG).show();

                     Intent i=new Intent(MessageActivity.this,FinalActivity.class);
                     startActivity(i);
                     }
                     else {
                     Toast.makeText(MessageActivity.this, "Permission denied", Toast.LENGTH_LONG).show();
                     }**/
                }


            }


        });

    }

    @Override
    public void onLocationChanged(Location location) {

        final String username=Prevalent.currentOnlineUser.getName();

        final double longitude=location.getLongitude();
        final double latitude=location.getLatitude();


        mref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                HashMap<String,Object> userdataMap=new HashMap<>();
                userdataMap.put("longitude",longitude);
                userdataMap.put("latitude",latitude);

                mref.child("UserLocations").child(username).updateChildren(userdataMap)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                                //Toast.makeText(MessageActivity.this, "Successfull.....", Toast.LENGTH_SHORT).show();
                            }
                        });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        Intent smsIntent = new Intent(Intent.ACTION_VIEW);

        smsIntent.setData(Uri.parse("smsto:"));
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address"  , new String ("83717502"));
        smsIntent.putExtra("sms_body"  ,  "My location is "+"Longitude : "+longitude+" latitude : "+latitude);

        try {
            startActivity(smsIntent);

            finish();
            Log.i("Finished sending SMS...", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MessageActivity.this,
                    "SMS faild, please try again later.", Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
